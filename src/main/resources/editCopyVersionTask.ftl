[@ui.bambooSection titleKey="com.atlassian.bamboo.plugins.variable.updater.variable.title"]
    [@ww.textfield labelKey="com.atlassian.bamboo.plugins.variable.updater.variable.origin" name="originVariable"
    required=true cssClass="long-field"/]
    [@ww.textfield labelKey="com.atlassian.bamboo.plugins.variable.updater.variable.destination" name="variable"
    required=true cssClass="long-field"/]
    [@ww.checkbox labelKey="com.atlassian.bamboo.plugins.variable.updater.variablecopy.customised" name="overrideCustomised" /]
[/@ui.bambooSection]

[@ui.bambooSection titleKey="com.atlassian.bamboo.plugins.variable.updater.scope.title"]
    [@ww.select labelKey='com.atlassian.bamboo.plugins.variable.updater.scope' name='variableScope'
        listKey='key' listValue='value' toggle='true' required=true
        list="variableScopeOptions" cssClass="long-field"]
    [/@ww.select]
    [@ui.bambooSection dependsOn='variableScope' showOn='PLAN']
        [@ww.checkbox labelKey="com.atlassian.bamboo.plugins.variable.updater.variable.globals" name="includeGlobals" /]
        [@ww.checkbox labelKey="com.atlassian.bamboo.plugins.variable.updater.variable.branch" name="branchVars" /]
    [/@ui.bambooSection]
[/@ui.bambooSection]
