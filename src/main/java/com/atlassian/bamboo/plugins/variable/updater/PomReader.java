package com.atlassian.bamboo.plugins.variable.updater;

import com.atlassian.bamboo.build.logger.BuildLogger;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class PomReader {

    final private Model model;

    public PomReader(@NotNull BuildLogger buildLogger,
                     @NotNull final File buildDirectory, @Nullable final String pomFile) throws IOException, XmlPullParserException {

        FileReader reader = null;
        try {
            if (pomFile != null && !pomFile.isEmpty()) {
                reader = new FileReader(new File(buildDirectory, pomFile));
            } else {
                reader = new FileReader(new File(buildDirectory, "pom.xml"));

            }
            final MavenXpp3Reader mavenreader = new MavenXpp3Reader();
            model = mavenreader.read(reader);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    buildLogger.addErrorLogEntry("help", e);
                }
            }
        }
    }

    public String retrieveValue() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        final String version = (String) PropertyUtils.getProperty(model, "version");
        if (version != null) {
            return version;
        }

        final String parent = (String) PropertyUtils.getProperty(model, "parent.version");
        if (parent != null) {
            return parent;
        }

        throw new IllegalArgumentException("Pom file does not have a valid version");
    }
}
